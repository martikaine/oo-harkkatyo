package databuilder;

import java.io.Serializable;

/**
 * Represents the coordinates of a point on the map.
 */
public class GeoPoint implements Serializable {

    /**
     * latitude as double
     */
    private final double latitude;

    /**
     * longitude as double
     */
    private final double longitude;


    GeoPoint(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    /**
     * @return  latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * @return  longitude
     */
    public double getLongitude() {
        return longitude;
    }

}