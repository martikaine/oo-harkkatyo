package databuilder;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.*;

/**
 * Builds a list of SmartPOST locations from an XML file when initialized.
 */
public class DataBuilder {
    /**
     * Holds all the SmartPost objects created.
     */
    private ArrayList<SmartPost> smartPostList = new ArrayList<>();

    /**
     * Holds an alphabetic list of cities read from the XML file.
     */
    private ArrayList<String> cityList = new ArrayList<>();

    /**
     * The list should be built on load.
     */
    public DataBuilder() {
        parseXMLData();
    }

    /**
     * Parses the XML file containing SmartPost data.
     * File location: http://smartpost.ee/fi_apt.xml
     */
    private void parseXMLData() {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = dbFactory.newDocumentBuilder();
            Document doc = documentBuilder.parse("http://smartpost.ee/fi_apt.xml");
            doc.getDocumentElement().normalize();

            NodeList nodes = doc.getElementsByTagName("place");

            for (int i = 0; i < nodes.getLength(); i++) {
                Element e = (Element) nodes.item(i);

                String code = getElementTextContent(e, "code");
                String city = getElementTextContent(e, "city");
                String address = getElementTextContent(e, "address");
                String postoffice = getElementTextContent(e, "postoffice");
                String availability = getElementTextContent(e, "availability");
                Double lat = Double.parseDouble(getElementTextContent(e, "lat"));
                Double lng = Double.parseDouble(getElementTextContent(e, "lng"));

                SmartPost s = new SmartPost(code, city, address, postoffice, availability, new GeoPoint(lat, lng));

                smartPostList.add(s);
                if (!cityList.contains(city)) cityList.add(city);
            }

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }

    private String getElementTextContent(Element e, String tag) {
        return e.getElementsByTagName(tag).item(0).getTextContent();
    }

    /**
     * Returns all SmartPosts from the specified city in an ArrayList.
     * @param city  City name to search for (str).
     * @return      ArrayList of matching SmartPost objects.
     */
    public ArrayList<SmartPost> findByCity(String city) {
        ArrayList<SmartPost> results = new ArrayList<>();

        for (SmartPost post : smartPostList) {
            if (Objects.equals(post.getCity(), city)) results.add(post);
        }
        return results;
    }

    /**
     * @return  smartPostList
     */
    public ArrayList<SmartPost> getSmartPostList() {
        return smartPostList;
    }

    /**
     * @return  cityList
     */
    public ArrayList<String> getCityList() {
        return cityList;
    }

}