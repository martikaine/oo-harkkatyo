package databuilder;

import java.io.Serializable;

/**
 * A data holder class for a SmartPost machine.
 */
public class SmartPost implements Serializable {

    /**
     * City code
     */
    private final String code;

    /**
     * City the office is located in
     */
    private final String city;

    /**
     * Street address
     */
    private final String address;

    /**
     * name of the office
     */
    private final String postOffice;

    /**
     * open hours (str)
     */
    private final String availability;

    /**
     * coordinates as a GeoPoint (dependency)
     */
    private final GeoPoint coordinates;



    /**
     * Constructor method, sets data fields
     */
    SmartPost(String code, String city, String address, String postOffice, String availability, GeoPoint coordinates) {
        this.code = code;
        this.city = city;
        this.address = address;
        this.postOffice = postOffice;
        this.availability = availability;
        this.coordinates = coordinates;
    }

    /**
     * Calculates shortest path distance (km) between two SmartPosts using the Haversine formula adapted from
     * http://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-longitude-points-haversine-formula
     * @param origin            Origin point
     * @param destination       Destination point
     * @return                  Distance in km.
     */
    public static int getDistanceBetweenOffices(SmartPost origin, SmartPost destination) {
        double R = 6371;
        double dLat = deg2rad(destination.getCoordinates().getLatitude() - origin.getCoordinates().getLatitude());
        double dLon = deg2rad(destination.getCoordinates().getLongitude() - origin.getCoordinates().getLongitude());
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(deg2rad(origin.getCoordinates().getLatitude()))
                * Math.cos(deg2rad(destination.getCoordinates().getLatitude())) * Math.sin(dLon/2) * Math.sin(dLon/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return (int) Math.round(R * c);
    }

    private static double deg2rad(double deg) {
        return deg * (Math.PI/180);
    }

    /**
     * @return  coordinates object
     */
    public GeoPoint getCoordinates() {
        return coordinates;
    }

    /**
     * @return  city
     */
    public String getCity() {
        return city;
    }

    /**
     * @return  Address of the office in a format conforming to the Google map API
     */
    public String getAddressString() {
        return address + ", " + code + " " + city;
    }

    /**
     * @return  Other info to be shown on the map as a string.
     */
    public String getInfoString() {
        return postOffice + ". Avoinna: " + availability;
    }

    /**
     * @return  String to be shown in a ComboBox with objects of this type
     */
    @Override
    public String toString() {
        return getAddressString();
    }
}