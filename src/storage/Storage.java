package storage;

import databuilder.SmartPost;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import packagelogger.PackageLogger;

import java.util.*;

/**
 * 
 */
public class Storage {

    /**
     * list of valid mailPackages ready to be sent out
     */
    private ObservableList<MailPackage> mailPackages = FXCollections.observableArrayList();

    /**
     * list of already used mailableObjects that can be packed
     */
    private ArrayList<MailableObject> mailableObjects = new ArrayList<>();

    /**
     * Default constructor
     */
    public Storage() {
        ArrayList<MailableObject> c = new ArrayList<>();
        c.add(new Obj1());
        c.add(new Obj2());
        c.add(new Obj3());
        c.add(new Obj4());
        c.add(new Obj5());
        mailableObjects.addAll(c);
    }

    /**
     * tries to pack an object using the specified package class, raising an error if it's not allowed
     * @param object            the MailableObject to be packed
     * @param packageClassNo    package class no: 1, 2 or 3
     * @param origin            SmartPost object for the package origin
     * @param destination       SmartPost object for the package destination
     */
    public void addPackage(MailableObject object, int packageClassNo, SmartPost origin, SmartPost destination) throws CannotBeSentException {
        MailPackage p;

        switch (packageClassNo) {
            case 1:
                p = new FirstClassPackage(object, origin, destination);
                break;
            case 2:
                p = new SecondClassPackage(object, origin, destination);
                break;
            case 3:
                p = new ThirdClassPackage(object, origin, destination);
                break;
            default:
                System.out.println("Invalid class!");
                return;
        }
        mailPackages.add(p);
    }

    /**
     * Removes a package from the storage, used when sending out a package
     * @param p     Package to be removed.
     */
    public void removePackage(MailPackage p) {
        mailPackages.remove(p);
    }

    /**
     * @return      mailPackages
     */
    public ObservableList<MailPackage> getMailPackages() {
        return mailPackages;
    }

    /**
     * @param name      Name of the object
     * @param width     Width (cm)
     * @param height    Height (cm)
     * @param depth     Depth (cm)
     * @param weight    Weight (kg)
     * @param isFragile Whether the object is fragile
     */
    public void addObject(String name, int height, int width, int depth, double weight, boolean isFragile) {
        MailableObject o = new MailableObject(name, height, width, depth, weight, isFragile);
        mailableObjects.add(o);
    }

    /**
     * @return  getter for mailableObjects
     */
    public ArrayList<MailableObject> getMailableObjects() {
        return mailableObjects;
    }

    public void saveStorageStatus() {
        ArrayList<MailPackage> list = new ArrayList<>(mailPackages);
        PackageLogger.serializeObject(list, "packages.ser");
    }

    public void loadStorageStatus() {
        ArrayList<MailPackage> list = (ArrayList) PackageLogger.deserializeObject("packages.ser");
        mailPackages = FXCollections.observableArrayList(list);
    }

}