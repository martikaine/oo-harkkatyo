package storage;

import databuilder.SmartPost;

/**
 *
 */
class SecondClassPackage extends MailPackage {

    private static int packageClassNo = 2;
    /**
     * Default constructor
     */
    SecondClassPackage(MailableObject object, SmartPost origin, SmartPost destination) throws CannotBeSentException {
        super(object, origin, destination);
    }

    @Override
    public int getPackageClassNo() {
        return SecondClassPackage.packageClassNo;
    }

    /**
     * constraints:
     * if object is fragile: max volume = 0,03 m^3 (~30x30x30cm)
     * @param object
     */
    protected void checkObjectCompatibility(MailableObject object) throws CannotBeSentException {
        if (object.getIsFragile() && object.getVolume() > 0.03)
            throw new CannotBeSentException("Särkyvä esineesi on liian suuri kuljetettavaksi 2. luokassa.");
    }

}