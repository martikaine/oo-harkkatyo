package storage;

import databuilder.SmartPost;


class FirstClassPackage extends MailPackage {

    private static final int packageClassNo = 1;

    /**
     * Default constructor
     */
    FirstClassPackage(MailableObject object, SmartPost origin, SmartPost destination) throws CannotBeSentException {
        super(object, origin, destination);
    }

    @Override
    public int getPackageClassNo() {
        return FirstClassPackage.packageClassNo;
    }

    /**
     * Constraints:
     * max distance: 150km
     * can't be fragile
     * @param object    object to be checked
     * @throws CannotBeSentException
     */
    protected void checkObjectCompatibility(MailableObject object) throws CannotBeSentException {
        if (travelDistance > 150)
            throw new CannotBeSentException("1.lk paketti voi kulkea enintään 150 km.");
        if (object.getIsFragile())
            throw new CannotBeSentException("1.lk paketti ei voi olla särkyvä.");
    }

}