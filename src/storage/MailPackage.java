package storage;

import databuilder.SmartPost;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * An abstract parent class for all package types.
 * Every package class has different constraints which are detailed in the spec.
 * When constructing a new package, it checks whether the object can be sent in this class,
 * raising a CannotBeSentException if it's not allowed.
 */
public abstract class MailPackage implements Serializable {

    /**
     * The object packed into this package.
     */
    protected final MailableObject object;

    /**
     * The SmartPost the package will be sent from.
     * Needed for checking send distance constraints.
     */
    private final SmartPost origin;

    /**
     * The SmartPost this package will be sent to.
     */
    private final SmartPost destination;

    /**
     * The distance between origin and destination.
     */
    final int travelDistance;

    /**
     * The package class of this package (1, 2 or 3).
     */
    private static int packageClassNo;

    /**
     * Constructor method.
     * Check if the object fits the constraints of the class with checkObjectCompability()
     * If it does, proceed
     * If it doesn't, raise an error.
     */
     MailPackage(MailableObject object, SmartPost origin, SmartPost destination) throws CannotBeSentException {
         this.origin = origin;
         this.destination = destination;
         this.travelDistance = SmartPost.getDistanceBetweenOffices(origin, destination);

         this.checkObjectCompatibility(object);
         this.object = object;
     }

    /**
     * @return  object in package
     */
    public MailableObject getObject() {
        return object;
    }

    /**
     * @return  origin
     */
    public SmartPost getOrigin() {
        return origin;
    }

    /**
     * @return  destination
     */
    public SmartPost getDestination() {
        return destination;
    }

    /**
     * @return  travelDistance
     */
    public int getTravelDistance() {
        return travelDistance;
    }


    public ArrayList<Double> getCoordinateArray() {
        ArrayList<Double> coords = new ArrayList<>();
        coords.add(origin.getCoordinates().getLatitude());
        coords.add(origin.getCoordinates().getLongitude());
        coords.add(destination.getCoordinates().getLatitude());
        coords.add(destination.getCoordinates().getLongitude());

        return coords;
    }
    /**
     * Implementation should return the static packageClassNo field of the subclass.
     * @return  packageClassNo
     */
    public abstract int getPackageClassNo();

    /**
     * Checks whether the object meets the constraints of the package class, throwing an error if not.
     * @param object    object to be checked
     */
    protected abstract void checkObjectCompatibility(MailableObject object) throws CannotBeSentException;

    /**
     * @return  String to be shown in a ComboBox with objects of this type
     */
    @Override
    public String toString() {
        return object.getName() + ", " + origin.getCity() + " - " + destination.getCity() + ", " + String.valueOf(getPackageClassNo()) + ". lk";
    }
}