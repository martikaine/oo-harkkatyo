package storage;

import java.io.Serializable;

/**
 * Data holder class for an object that can be packed to a package.
 */
public class MailableObject implements Serializable {

    /**
     * Name of the object
     */
    private final String name;

    /**
     * Height (cm)
     */
    private final int height;

    /**
     * Width (cm)
     */
    private final int width;

    /**
     * Depth (cm)
     */
    private final int depth;

    /**
     * Weight (kg)
     */
    private final double weight;

    /**
     * Whether the object is fragile
     */
    private final boolean isFragile;


    /**
     * Constructor sets values to the data fields
     */
    MailableObject(String name, int height, int width, int depth, double weight, boolean isFragile) {
        this.name = name;
        this.height = height;
        this.width = width;
        this.depth = depth;
        this.weight = weight;
        this.isFragile = isFragile;
    }

    /**
     * @return  name
     */
    public String getName() {
        return name;
    }

    /**
     * @return  height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @return  width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @return  depth
     */
    public int getDepth() {
        return depth;
    }

    /**
     * @return  volume in m^3
     */
    public int getVolume() {
        return height*width*depth / 1000000;
    }

    /**
     * @return  weight
     */
    public double getWeight() {
        return weight;
    }

    /**
     * @return  isFragile
     */
    public boolean getIsFragile() {
        return isFragile;
    }

    /**
     * @return  String to be shown in a ComboBox with objects of this type
     */
    @Override
    public String toString() {
        return name + ", " + String.valueOf(height) +"*"+ String.valueOf(width) +"*"+ String.valueOf(depth) +"cm, "+ String.valueOf(weight) + " kg" + ((isFragile) ? " - SÄRKYVÄ" : "");
    }
}

class Obj1 extends MailableObject {
    Obj1() {
        super("C-kasetti", 10, 6, 1, 0.1, false);
    }
}

class Obj2 extends MailableObject {
    Obj2() {
        super("Kananmunia", 30, 8, 5, 0.2, true);
    }
}

class Obj3 extends MailableObject {
    Obj3() {
        super("Joulukuusi", 200, 50, 50, 5.0, false);
    }
}

class Obj4 extends MailableObject {
    Obj4() {
        super("Keilapallo", 20, 20, 20, 6.5, false);
    }
}

class Obj5 extends MailableObject {
    Obj5() {
        super("Saksofoni", 50, 20, 10, 3.5, true);
    }
}
