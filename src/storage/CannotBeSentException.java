package storage;


public class CannotBeSentException extends Exception {
    CannotBeSentException(String message) {
        super(message);
    }
}
