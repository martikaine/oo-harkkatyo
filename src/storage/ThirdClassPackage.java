package storage;

import databuilder.SmartPost;


class ThirdClassPackage extends MailPackage {

    private static int packageClassNo = 3;
    /**
     * Default constructor
     */
    ThirdClassPackage(MailableObject object, SmartPost origin, SmartPost destination) throws CannotBeSentException {
        super(object, origin, destination);
    }

    /**
     * @return packageClassNo
     */
    @Override
    public int getPackageClassNo() {
        return ThirdClassPackage.packageClassNo;
    }

    /**
     * Constraints:
     * min weight: 25 kg
     * min volume: 1 m^3
     * can't be fragile
     */
    protected void checkObjectCompatibility(MailableObject object) throws CannotBeSentException {
        if (object.getWeight() < 25)
            throw new CannotBeSentException("3. luokassa ei kuljeteta alle 25 kg paketteja.");
        if (object.getIsFragile())
            throw new CannotBeSentException("3.lk lähetys ei voi olla särkyvä.");
        if (object.getVolume() < 1)
            throw new CannotBeSentException("3. luokassa ei kuljeteta paketteja, joiden tilavuus on alle 1 m^3.");
    }

}