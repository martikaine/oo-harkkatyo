package timo;

import databuilder.DataBuilder;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import packagelogger.PackageLogger;
import storage.Storage;

public class Main extends Application {
    private PackageLogger logger = new PackageLogger();
    private Storage storage = new Storage();
    private DataBuilder db = new DataBuilder();

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("timo.fxml"));
        Parent root = loader.load();

        // inject dependencies to controller
        TIMOController controller = loader.getController();
        controller.setServices(logger, storage, db);

        // show window
        primaryStage.setTitle("TIMO");
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Fires on window exit. Save log and storage status to disk.
     * @throws Exception
     */
    @Override
    public void stop() throws Exception {
        logger.saveLogToFile();
        storage.saveStorageStatus();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
