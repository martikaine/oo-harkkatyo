package timo;


import databuilder.*;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import packagelogger.*;
import storage.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class TIMOController {
    public TableColumn<LoggedAction, String> logItemColumn;
    public TableColumn<LoggedAction, String> logOriginColumn;
    public TableColumn<LoggedAction, String> logDestColumn;
    public TableColumn<LoggedAction, Integer> logDistanceColumn;
    public TableColumn<LoggedAction, Integer> logClassColumn;
    public TableColumn<LoggedAction, Date> logTimestampColumn;

    public TableColumn<MailPackage, String> pkgItemColumn;
    public TableColumn<MailPackage, String> pkgOriginColumn;
    public TableColumn<MailPackage, String> pkgDestColumn;
    public TableColumn<MailPackage, Integer> pkgDistanceColumn;
    public TableColumn<MailPackage, Integer> pkgClassColumn;
    public TabPane tabPane;

    @FXML
    private Label classInfoLabel;
    @FXML
    private WebView mapWebView;
    @FXML
    private ComboBox<String> citySelectBox;
    @FXML
    private ComboBox<MailPackage> packageSelectBox;
    @FXML
    private ComboBox<MailableObject> objectSelectBox;
    @FXML
    private ComboBox<SmartPost> destinationBox;
    @FXML
    private ComboBox<SmartPost> originBox;
    @FXML
    private Label distanceLbl;
    @FXML
    private RadioButton firstClassRadio;
    @FXML
    private RadioButton secondClassRadio;
    @FXML
    private RadioButton thirdClassRadio;
    @FXML
    private TableView<MailPackage> packageTable;
    @FXML
    private TableView<LoggedAction> logTable;


    private PackageLogger logger;
    private Storage storage;
    private DataBuilder db;


    @FXML
    public void initialize() {
        // load map in webview
        mapWebView.getEngine().load(getClass().getResource("index.html").toExternalForm());

        // set up packages table
        pkgItemColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getObject().getName()));
        pkgOriginColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getOrigin().getAddressString()));
        pkgDestColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getDestination().getAddressString()));
        pkgDistanceColumn.setCellValueFactory(cellData -> new SimpleIntegerProperty(cellData.getValue().getTravelDistance()).asObject());
        pkgClassColumn.setCellValueFactory(cellData -> new SimpleIntegerProperty(cellData.getValue().getPackageClassNo()).asObject());

        // set up log table
        logItemColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getObjectName()));
        logOriginColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getOrigin()));
        logDestColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getDestination()));
        logDistanceColumn.setCellValueFactory(cellData -> new SimpleIntegerProperty(cellData.getValue().getDistance()).asObject());
        logClassColumn.setCellValueFactory(cellData -> new SimpleIntegerProperty(cellData.getValue().getPackageClass()).asObject());
        logTimestampColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getTimestamp()));
    }

    /**
     * Inject dependencies into the controller. Always set these before showing the scene.
     * @param logger        Logger instance
     * @param storage       Storage instance
     * @param db            DataBuilder instance
     */
    void setServices(PackageLogger logger, Storage storage, DataBuilder db) {
        boolean firstRun = false;
        if (this.logger == null) firstRun = true;

        this.logger = logger;
        this.storage = storage;
        this.db = db;

        if(firstRun) {          // initialization tasks that rely on these dependencies
            logger.loadLogFromFile();
            storage.loadStorageStatus();

            logTable.setItems(logger.getLog());
            packageTable.setItems(storage.getMailPackages());

            updateComboBox(citySelectBox, db.getCityList());
            updateComboBox(originBox, db.getSmartPostList());
            updateComboBox(destinationBox, db.getSmartPostList());
            updateComboBox(packageSelectBox, storage.getMailPackages());
            updateComboBox(objectSelectBox, storage.getMailableObjects());
        }
    }

    /**
     * Updates a ComboBox with values from the specified ArrayList.
     * @param box   ComboBox to be updated
     * @param list  List to pull data from
     * @param <T>   both must contain objects of the same type
     */
    private <T> void updateComboBox(ComboBox<T> box, List<T> list) {
        box.getItems().clear();
        box.getItems().addAll(list);
    }

    /**
     * Displays the "create new object" popup window. Updates the objects ComboBox on closure.
     */
    @FXML
    private void showNewObjectPopup() {
        try {
            Stage popup = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("newObjectPopup.fxml"));
            Parent page = loader.load();

            // dependency injection
            NewObjectPopupController controller = loader.getController();
            controller.setStorage(storage);

            // show window
            Scene scene = new Scene(page);
            popup.setScene(scene);
            popup.setTitle("Luo esine");
            popup.show();

            // set window closure event listener
            popup.setOnHiding(we -> updateComboBox(objectSelectBox, storage.getMailableObjects()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds markers to the map.
     */
    @FXML
    private void addMarkers() {
        ArrayList<SmartPost> results = db.findByCity(citySelectBox.getValue());

        for(SmartPost p : results) {
            mapWebView.getEngine().executeScript(
                    "document.goToLocation('"+p.getAddressString()+"', '"+p.getInfoString()+"', 'blue')"
            );
        }
    }

    /**
     * Deletes all paths from the map.
     */
    @FXML
    private void clearPaths() {
        mapWebView.getEngine().executeScript("document.deletePaths()");
    }

    /**
     * Shows info text for the selected package class in the side panel.
     */
    @FXML
    private void showClassInfo() {
        String infoText;
        if (firstClassRadio.isSelected()) {
            infoText = "1. luokan paketti on kaikista nopein pakettiluokka, jonka vuoksi sitä ei voi lähettää " +
                    "pidemmälle kuin 150 km päähän. Onhan yleisesti tiedossa, että sen pidempää matkaa ei TIMO-mies " +
                    "jaksa pakettia kuljettaa. 1. luokan paketti on myös nopea, koska sen turvallisuudesta ei välitetä " +
                    "niin paljon, jolloin kaikki särkyvät esineet tulevat menemään rikki matkan aikana.";
        } else if (secondClassRadio.isSelected()) {
            infoText = "2. luokan paketit ovat turvakuljetuksia, jolloin ne kestävät parhaiten kaiken särkyvän tavaran " +
                    "kuljettamisen. Näitä paketteja on mahdollista kuljettaa jopa Lapista Helsinkiin, sillä matkan " +
                    "aikana käytetään useampaa kuin yhtä TIMO-miestä, jolloin turvallinen kuljetus on taattu. " +
                    "Paketissa on kuitenkin huomattava, että jos se on liian suuri, ei särkyvä esine voi olla " +
                    "heilumatta, joten paketin koon on oltava pienempi kuin muilla pakettiluokilla.";
        } else {
            infoText = "3. luokan paketti on TIMO-miehen stressinpurkupaketti. Tämä tarkoittaa sitä, että " +
                    "TIMO-miehellä ollessa huono päivä pakettia paiskotaan seinien kautta automaatista toiseen, " +
                    "joten paketin sisällön on oltava myös erityisen kestävää materiaalia. Myös esineen suuri koko ja " +
                    "paino ovat eduksi, jolloin TIMO-mies ei jaksa heittää pakettia seinälle kovin montaa kertaa. " +
                    "Koska paketit päätyvät kohteeseensa aina seinien kautta, on tämä hitain mahdollinen " +
                    "kuljetusmuoto paketille. ";
        }

        classInfoLabel.setText(infoText);
    }


    /**
     * Reads fields from the create package form and tries to create a new package accordingly.
     * Displays an error message if unsuccessful.
     */
    @FXML
    private void createPackage() {
        int pkgClassNo;
        if (firstClassRadio.isSelected()) {
            pkgClassNo = 1;
        } else if (secondClassRadio.isSelected()) {
            pkgClassNo = 2;
        } else {
            pkgClassNo = 3;
        }

        // Attempt to create the package, displaying an error alert to the user if it cannot be sent in the selected class
        try {
            storage.addPackage(objectSelectBox.getValue(), pkgClassNo, originBox.getValue(), destinationBox.getValue());
        } catch (CannotBeSentException ex) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Pakettia ei voi lähettää tässä luokassa: " + ex.getMessage());
            a.show();
        }

        updateComboBox(packageSelectBox, storage.getMailPackages());
    }

    /**
     * Updates the distance label in the new package form.
     */
    @FXML
    private void updateDistanceLabel() {
        if ((originBox.getValue() != null) && (destinationBox.getValue() != null)) {
            int dist = SmartPost.getDistanceBetweenOffices(originBox.getValue(), destinationBox.getValue());
            distanceLbl.setText(String.valueOf(dist) + " km");
        }
    }

    /**
     * Send a package.
     * Draws the path on the map, removes the package from storage and logs the action.
     */
    @FXML
    private void sendPackage() {
        MailPackage sent = packageSelectBox.getValue();

        // set line color depending on package class
        String color;
        switch (sent.getPackageClassNo()) {
            case 1:     color = "purple";
                        break;
            case 2:     color = "blue";
                        break;
            default:    color = "red";
                        break;
        }

        // draw path using map API
        mapWebView.getEngine().executeScript(
                "document.createPath("+ sent.getCoordinateArray() + ", '"+ color +"', '" + sent.getPackageClassNo() + "')"
        );

        // remove from storage, update GUI to reflect
        storage.removePackage(sent);
        updateComboBox(packageSelectBox, storage.getMailPackages());

        // logging
        logger.addAction(
                sent.getOrigin().getAddressString(),
                sent.getDestination().getAddressString(),
                sent.getTravelDistance(),
                sent.getObject().getName(),
                sent.getPackageClassNo()
        );
    }

    @FXML
    private void goToStorageTab() {
        SingleSelectionModel<Tab> selectionModel = tabPane.getSelectionModel();
        selectionModel.select(1);
    }

    @FXML
    private void goToMapTab() {
        SingleSelectionModel<Tab> selectionModel = tabPane.getSelectionModel();
        selectionModel.select(0);
    }
}
