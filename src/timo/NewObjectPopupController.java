package timo;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import storage.Storage;

import java.util.Calendar;
import java.util.Objects;


public class NewObjectPopupController {
    @FXML
    private TextField nameField;
    @FXML
    private TextField widthField;
    @FXML
    private TextField depthField;
    @FXML
    private TextField heightField;
    @FXML
    private TextField weightField;
    @FXML
    private Button createObjectBtn;
    @FXML
    private CheckBox fragileCheckbox;

    private Storage storage;

    /**
     * Give the storage instance to the controller. Must be set before showing the scene.
     * @param storage   storage instance
     */
    void setStorage(Storage storage) {
        this.storage = storage;
    }

    /**
     * Reads fields from the create object form, validates them and creates a new object accordingly.
     */
    @FXML
    private void createObject() {
        //TODO validation
        String name = nameField.getText();
        int height = Integer.parseInt(heightField.getText());
        int width = Integer.parseInt(widthField.getText());
        int depth = Integer.parseInt(depthField.getText());
        double weight = Double.parseDouble(weightField.getText());
        boolean isFragile = fragileCheckbox.isSelected();

        if (Objects.equals(name, "Doge")) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Nykyinen vuosi on: " + Calendar.getInstance().get(Calendar.YEAR));
            a.show();
        } else {
            storage.addObject(name, height, width, depth, weight, isFragile);
        }

        closeWindow();
    }

    /**
     * Closes the window.
     */
    @FXML
    private void closeWindow() {
        Stage stage = (Stage) createObjectBtn.getScene().getWindow();
        stage.close();
    }
}

