package packagelogger;

import java.io.Serializable;
import java.util.*;

/**
 * Data holder object for an action stored in the log.
 */
public class LoggedAction implements Serializable {

    /**
     * 
     */
    private final String origin;

    /**
     * 
     */
    private final String destination;

    /**
     * 
     */
    private final int distance;

    /**
     * 
     */
    private final String objectName;

    /**
     * 
     */
    private final int packageClass;

    /**
     * 
     */
    private final Date timestamp;


    /**
     * Constructor method.
     */
    LoggedAction(String origin, String destination, int distance, String objectName, int packageClass) {
        this.origin = origin;
        this.destination = destination;
        this.distance = distance;
        this.objectName = objectName;
        this.packageClass = packageClass;
        this.timestamp = new Date();
    }

    /**
     * @return  origin
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * @return  destination
     */
    public String getDestination() {
        return destination;
    }

    /**
     * @return  distance
     */
    public int getDistance() {
        return distance;
    }

    /**
     * @return  objectName
     */
    public String getObjectName() {
        return objectName;
    }

    /**
     * @return  packageClass
     */
    public int getPackageClass() {
        return packageClass;
    }

    /**
     * @return timestamp
     */
    public Date getTimestamp() {
        return timestamp;
    }

}