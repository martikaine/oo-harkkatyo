package packagelogger;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.*;
import java.util.*;

/**
 * Singleton
 */
public class PackageLogger {

    /**
     * List of logged actions
     */
    private ObservableList<LoggedAction> log = FXCollections.observableArrayList();


    /**
     * @return  log
     */
    public ObservableList<LoggedAction> getLog() {
        return log;
    }

    /**
     * Adds a new action to the log.
     * @param origin        Origin of the package
     * @param destination   Destination of the package
     * @param distance      Distance travelled (km)
     * @param objectName    Name of the object inside the package
     * @param packageClass  The package's class
     */
    public void addAction(String origin, String destination, int distance, String objectName, int packageClass) {
        LoggedAction a = new LoggedAction(origin, destination, distance, objectName, packageClass);
        log.add(a);
    }

    /**
     * Saves the log by serializing the log object.
     * ObservableList is not serializable so the object is converted to a normal ArrayList before saving.
     */
    public void saveLogToFile() {
        System.out.println("Saving log.");
        // convert to ArrayList
        ArrayList<LoggedAction> converted = new ArrayList<>();
        converted.addAll(log);
        serializeObject(converted, "log.ser");
    }

    /**
     * Parses the log file back to LoggedAction objects.
     */
    public void loadLogFromFile() {
        System.out.println("Loading log.");

        ArrayList<LoggedAction> list = (ArrayList<LoggedAction>) deserializeObject("log.ser");
        // convert back to ObservableList
        log = FXCollections.observableArrayList(list);
    }

    /**
     * Serializes an object.
     * @param o         A serializable object
     * @param filename  file name
     */
    public static void serializeObject(Serializable o, String filename) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename));
            out.writeObject(o);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Deserializes an object.
     * @param filename  file to read from
     * @return          deserialized object/null if cannot read
     */
    public static Object deserializeObject(String filename) {
        Object o = null;
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename));
            o = in.readObject();
            in.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return o;
    }
}